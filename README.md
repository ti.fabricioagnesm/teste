# Projeto Teste
Para esse projeto executar é necessário ter node e npm instalados. Além do git para clonar o projeto

## Instalação
Para executar o projeto serão necessarios os seguintes passos:

```bash
git clone https://gitlab.com/ti.fabricioagnesm/teste.git
```
 - Após, acesse a pasta do projeto


```bash
npm install
```
 - Instala componentes necessários


```bash
npm run build
```
 - Gera a build do código


```bash
node ./server.js
```
 - Roda o servidor.
 
Após esses passos o projeto estara disponível em http://localhost:4000/