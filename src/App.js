import React, { Component } from 'react'
import { Map, GoogleApiWrapper, Marker } from 'google-maps-react'

export class MapContainer extends Component {
  // -- Construtor
  constructor (props) {
    super(props)

    this.state =
    {
      stores: [
        { latitude: -30.029880792541796, longitude: -51.20700819824219 },
        { latitude: -30.03842597816721, longitude: -51.23155577514649 }
      ]
    }
  }

  // -- Exibe marcadores no mapa
  displayMarkers () {
    return this.state.stores.map((store, index) => {
      return <Marker key={index} id={index} position={{ lat: store.latitude, lng: store.longitude }} />
    })
  }

  // -- Lista os marcadores
  listMarkers () {
    return this.state.stores.map((store, index) => {
      return <li key={index} id={index}> {store.latitude}{store.longitude}</li>
    }
    )
  }

  // -- Adicionar marcadores ao mapa
  addMarkers = (mapProps, map, clickEvent) => {
    return this.setState(state => {
      this.state.stores.push({ latitude: clickEvent.latLng.lat(), longitude: clickEvent.latLng.lng() })
      return { state }
    })
  };

  // -- Executado ao render
  render () {
    return (
      <div className='main'>
        <div className='container'>
          <div className='row '>
            <div className='col-md-8 vh-90'>
              <h2>Mapa</h2>
              <Map
                google={this.props.google}
                zoom={14}
                style={{ width: '80%', height: '80%' }}
                initialCenter={{ lat: -30.0290332, lng: -51.2296675 }}
                onClick={this.addMarkers}
              >
                {this.displayMarkers()}
              </Map>
            </div>
            <div className='col-md-4'>
              <h2>Pontos já selecionados</h2>
              <div>
                <ul>
                  {this.listMarkers()}
                </ul>
              </div>
            </div>
          </div>
        </div>
      </div>
    )
  }
}

export default GoogleApiWrapper({
  apiKey: 'AIzaSyCHe3CDLuqcPnOy-xlVxFagD950ZyUpvSA'
})(MapContainer)
